> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS 4368

## Cray Clark

### Assignment # Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Development Installation
    - Questions
    - Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Install mysql
    * Create Sample web app 
    * Use WebServlet to deploy Webapp

3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Install Mysql Workbench
	* Create ERD with provided specifications
	* Foward Engineer Database
	* Create A3 README.md file

4. [P1 README.md](p1/README.md "My P1 README.md file")
	* Clone Starter files from bitbucket
	* Add form controls for Street, City, State, Zip, Phone, Balance, and Total Sales
	* Add jQuery validations for each catagory
	* Create P1 Readme file 

5. [A4 README.md](a4/README.md "My A4 README.md file")
	* Clone starter files from bitbucket
	* Edit the server side controls to add Street, City, State, Zip, Phone, Balance, Total Sales, and Notes
	* Create A4 README file

6. [A5 README.md](a5/README.md "My A5 README.md file")
	* Create ConnectionPool file, CustomerDB file, and DBUtil file.
	* Edit the server side controls to prevent sql injection.
	* Create README file

7. [P2 README.md](p2/README.md "My P2 README.md file")
	* Add funtionality to show current data
	* Finshed CRUD Functionality
	* Create README file