> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Cray Clark

### Assignment #3 Requirements:

*Four Parts:*

1. Install Mysql Workbench
2. Create ERD with provided Specifications 
3. Forward Engineer Database
4. Create Readme file 


#### README.md file should include the following items:

* Screenshot of ERD
* Mysql Workbench file



#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD](img/erd.png)



#### Assessment Links:
* [Mysql Workbench File](a3.mwb)
* [A3 sql File](a3.sql)




