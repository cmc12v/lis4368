> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Cray Clark

### Assignment #5 Requirements:

*Three Parts:*

1. Create ConnectionPool file, CustomerDB file, and DBUtil file.
2. Edit the server side controls to prevent sql injection.
3. Create README file


#### README.md file should include the following items:

* Screenshot of Entered Data 
* Screenshot of succesful validation
* Screenshot of Data Populating


#### Assignment Screenshots:

*Screenshot of Entered Data*:

![Entered Data](img/formentry.png)

*Screenshot of Passed Validation*:

![Passed Validation](img/validation.png)

*Screenshot of Data populating*:

![Data Populating](img/sql.png)

