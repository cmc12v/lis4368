> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Cray Clark

### Assignment #4 Requirements:

*Three Parts:*

1. Clone starter files from bitbucket
2. Edit the server side controls to add Street, City, State, Zip, Phone, Balance, Total Sales, and Notes
3. Create README file


#### README.md file should include the following items:

* Screenshot of Failed Validation 
* Screenshot of succesful validation



#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![Failed Validation](img/failed.png)

*Screenshot of Passed Validation*:

![Passed Validation](img/success.png)



