> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Cray Clark

### Project #2 Requirements:

*Three Parts:*

1. Add funtionality to show current data
2. Finshed CRUD Functionality
3. Create README file

#### README.md file should include the following items:

* Screenshot of Entered Data 
* Screenshot of succesful validation
* Screenshot of Displayed Data
* Screenshot of Modify Form
* Screenshot of Modified Data
* Screenshot of Delete Warning
* Screenshot of Associated Database Changes

#### Assignment Screenshots:

*Screenshot of Entered Data*:

![Entered Data](img/validentry.png)

*Screenshot of Passed Validation*:

![Passed Validation](img/passedvalidation.png)

*Screenshot of Displayed Data *:

![Displayed Data](img/displaydata.png)

*Screenshot of Modify Form *:

![Modify Form](img/modifydata.png)

*Screenshot of Modified Data *:

![Modified Data](img/dmodsuccess.png)

*Screenshot of Delete Warning *:

![Delete Warning](img/deletewarning.png)

*Screenshot of Data Changes *:

![Data Changes](img/datachanges.png)
