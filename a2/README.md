> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Cray Clark

### Assignment #2 Requirements:

*Two Parts:*

1. Install mysql
2. Create Sample web app 


#### README.md file should include the following items:

* Screenshot of Query Results
* Assessment Links



#### Assignment Screenshots:

*Screenshot of query results running http://localhost:9999/hello/query?author=Tan+Ah+Teck*:

![Query Results Screenshot](img/queryresults.png)



#### Assessment Links:
* [http://localhost:9999/hello](http://localhost:9999/hello)
* [http://localhost:9999/hello/Hellohome.html](http://localhost:9999/hello/Hellohome.html)
* [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
* [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
* [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)





