> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Cray Clark

### Project #1 Requirements:


1. Clone Starter files from bitbucket
2. Add form controls for Street, City, State, Zip, Phone, Balance, and Total Sales
3. Add jQuery validations for each catagory
4. Create Readme file 


#### README.md file should include the following items:

* Screenshot of Online Portfolio Homepage
* Screenshot of failed validation
* Screenshot of passed validation



#### Assignment Screenshots:

*Screenshot of Portfolio Homepage*:

![Homepage](img/homepage.png)

*Screen shot of Failed Validation*:

![Failed Validation](img/failedvalidation.png)

*Screen shot of Passed Validation*:

![Passed Validation](img/passedvalidation.png)






